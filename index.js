var express = require('express');
var app = express();
var db = require('./db.js');
var bodyParser = require("body-parser");
app.use(bodyParser.json());

app.get('/', function(req, res) {
	res.end("Hello world!");
});

app.get('/app', function(req, res) {
	db.query("SELECT * FROM app", function (err, result) {
		if (!err) {
			res.json(result.rows);
		} else {
			res.send("Error a la db");
			console.log("error");
		}
	});
});
app.get('/app/:code', function(req, res) {
        db.query("SELECT * FROM app  where app_code = $1", [req.params.code], function (err, result) {
                if (!err) {
                        res.json(result.rows);
                } else {
                        res.send("Error a la db");
                        console.log("error");
                }
        });
});

app.post('/app/:code', function(req, res) {	//Name of app cannot have more than 5 letters
	db.query("INSERT INTO app (app_code, description) values($1, $2)", [req.params.code, req.body.description], function (err, result) {
		if (!err) {
		        res.json(result.rows);
                } else {
                        res.send("Error a la db");
                        console.log("error");
                }

	})
})


app.get('/record', function(req, res) {
        db.query("SELECT * FROM record", function (err, result) {
                if (!err) {
                        res.json(result.rows);
                } else {
                        res.send("Error a la db");
                        console.log("error");
                }
        });
});
/*app.get('/record/:code', function(req, res) {
        db.query("SELECT * FROM record  where id = "+req.params.code, function (err, result) {
                if (!err) {
                        res.json(result.rows);
                } else {
                        res.send("Error a la db");
                        console.log(error);
                }
        });
});*/
app.get('/record/app/:code', function(req, res) {
        db.query("SELECT * FROM record  where app_code = $1", [req.params.code], function (err, result) {
                if (!err) {
                        res.json(result.rows);
                } else {
                        res.send("Error a la db");
                        console.log("error");
                }
        });
});
app.get('/record/player/:code', function(req, res) {
        db.query("SELECT * FROM record  where player = $1", [req.params.code], function (err, result) {
                if (!err) {
                        res.json(result.rows);
                } else {
                        res.send("Error a la db");
                        console.log("error");
                }
        });
});

app.post('/record', function(req, res) {	//Name of app cannot have more than 5 letters
	db.query("INSERT INTO record (app_code, player, score) values($1, $2, $3)", [req.body.app_code, req.body.player, req.body.score], function (err, result) {
		if (!err) {
		        res.json(result.rows);
                } else {
                        res.send("Error a la db");
                        console.log("error");
                }

	})
})

app.listen(process.env.PORT || 3000, function () {
	console.log("Example app listening");
});
